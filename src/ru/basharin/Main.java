package ru.basharin;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Main {

    private static Random random = new Random();
    private static List<Integer> numbersList = new ArrayList<>();

    public static void addRandomNumbers() {
        int n = 1000;
        for (int i = 0; i < n; i++) {
            int j = random.nextInt(100000);
            numbersList.add(j);
        }
    }

    public static void main(String[] args) throws IOException, TransformerException, SQLException, ParserConfigurationException, SAXException, InterruptedException {
        addRandomNumbers();
        JDBCConnection jdbcConnection = new JDBCConnection();
        Date date1 = new Date();
        System.out.println(date1.getTime());
        jdbcConnection.writeInDB(numbersList);
        Date date = new Date();
        System.out.println(date.getTime());
        WorkWithFile workWithFile = new WorkWithFile();
        workWithFile.createDocument();
        workWithFile.writeParamXML();
        workWithFile.convertXMLtoXML("1.xml", "convert.xsl");
        workWithFile.parseDoc();
    }
}
