package ru.basharin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class JDBCConnection {

    private Connection connection = null;

    public Connection init() {
        try {
            Class.forName("org.sqlite.JDBC");
//            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\drbah\\Downloads\\bitbucket\\Task\\TestProject\\test.db")
            connection = DriverManager.getConnection("jdbc:sqlite:C:test.db");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void writeInDB(List<Integer> numbersList) throws IOException {
        String sqlInsert = "insert into test(field) values(?);";
        String sqlDelete = "delete from test where field>0;";

        try (Connection connection = init();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try (Connection connection = init();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert)) {
            for (int num : numbersList) {
                preparedStatement.setInt(1, num);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
