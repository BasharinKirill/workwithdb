package ru.basharin;

import java.io.Serializable;
import java.util.Objects;

public class Numbers implements Serializable {
    private int number;
    private String name;
    private String password;

    public Numbers() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Numbers numbers = (Numbers) o;
        return number == numbers.number &&
                Objects.equals(name, numbers.name) &&
                Objects.equals(password, numbers.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name, password);
    }

    @Override
    public String toString() {
        return "Numbers{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
