package ru.basharin;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WorkWithFile {
    private DocumentBuilder documentBuilder;
    private JDBCConnection jdbcConnection = new JDBCConnection();

    public void createDocument() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void writeParamXML() throws TransformerException, FileNotFoundException {
        String selectSQL = "select * from test";
        Document document = documentBuilder.newDocument();
        Element entries = document.createElement("entries");
        try (Connection connection = jdbcConnection.init();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(selectSQL)) {
            while (resultSet.next()) {
                Element entry = document.createElement("entry");
                Element field = document.createElement("field");
                field.appendChild(document.createTextNode(resultSet.getString(1)));
                entries.appendChild(field);
                entries.appendChild(entry);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        document.appendChild(entries);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(document), new StreamResult(new FileOutputStream("1.xml")));
    }

    public void convertXMLtoXML(String xmlFile, String xslFile) throws FileNotFoundException, TransformerException {
        InputStream xml = new FileInputStream(xmlFile);
        InputStream xsl = new FileInputStream(xslFile);
        StreamSource xmlSourse = new StreamSource(xml);
        StreamSource stylesourse = new StreamSource(xsl);
        FileOutputStream fileOutputStream = new FileOutputStream("2.xml");
        StreamResult xmlOutput = new StreamResult(fileOutputStream);
        Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesourse);
        transformer.transform(xmlSourse, xmlOutput);
    }

    public void parseDoc() throws ParserConfigurationException, IOException, SAXException {
        List<Integer> temp = new ArrayList<>();
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse("2.xml");
        Node root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            nodeList.item(i);
            temp.add(Integer.parseInt(String.valueOf(nodeList.item(i).getAttributes().item(0).getTextContent())));
        }
        int sum = 0;
        for (int k = 0; k < temp.size(); k++) {
            sum += temp.get(k);
        }
        System.out.println("Сумма чисел " + sum);
    }
}
